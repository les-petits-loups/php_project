<?php

namespace App\Database;

use App\Core\Controller\AbstractController;
use App\Entity\Question;
use App\Entity\PossibleAnswer;
use PDOException;

class PossibleAnswerDB extends AbstractController
{
    private $pdo;
    private $pdoStatement;

    public function __construct()
    {
        $this->pdo = $this->getConnection();
    }

    public function getAnswersByQuestion(Question $question)
    {
        $id = $question->getId();
        $query = 'SELECT * FROM `PossibleAnswer` WHERE question=:id';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['id'] = intval($value['id']);
                    $value['question'] = $question;
                    $result[] = new PossibleAnswer($value);
                }
    
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function addPossibleAnswer(PossibleAnswer $answer): bool
    {
        $questionId = $answer->getQuestion()->getId();
        $label = $answer->getLabel();
        $isRight = $answer->getIsRight();

        $query = 'INSERT INTO PossibleAnswer(question, label, isRight)
        VALUES(:question, :label, :isRight)';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('question', $questionId, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('label', $label, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('isRight', $isRight, \PDO::PARAM_BOOL);

            $valid = $this->pdoStatement->execute();
            
            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function updatePossibleAnswer(PossibleAnswer $answer): bool
    {
        $questionId = $answer->getQuestion()->getId();
        $label = $answer->getLabel();
        $isRight = $answer->getIsRight();
        $id = $answer->getId();

        $query = 'UPDATE PossibleAnswer 
                SET label=:label,
                isRight=:isRight,
                question=:question
                WHERE id=:id';
       
        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('question', $questionId, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('label', $label, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('isRight', $isRight, \PDO::PARAM_BOOL);
            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);


            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }
}
