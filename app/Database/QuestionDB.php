<?php

namespace App\Database;

use App\Core\Controller\AbstractController;
use App\Entity\Question;
use PDOException;

class QuestionDB extends AbstractController
{
    private $pdo;
    private $pdoStatement;

    public function __construct()
    {
        $this->pdo = $this->getConnection();
    }

    public function isQuestionUnique(string $label, int $id): bool {
        $query = 'SELECT * FROM `Question` WHERE label = ? AND id <> ?';
        
        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute([$label, $id]);
            
            if ($valid) {
                if ($this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    return false;
                }
            }
            return true;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function addQuestion(Question $question)
    {
        $label = $question->getLabel();
        $themeId = $question->getTheme()->getId();

        $query = 'INSERT INTO Question(label, theme)
        VALUES(:label, :theme)';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('label', $label, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('theme', $themeId, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                return $this->getConnection()->lastInsertId();
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function deleteQuestionById(int $id): bool 
    {
        $query = 'DELETE FROM Question WHERE id=:id';
    
        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function updateQuestion(Question $question): bool
    {
        $label = $question->getLabel();
        $themeId = $question->getTheme()->getId();
        $id = $question->getId();

        $query = 'UPDATE Question 
                SET label=:label,
                theme=:theme
                WHERE id=:id';
       
        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('label', $label, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('theme', $themeId, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getAll(int $page)
    {
        $themeDB = new ThemeDB();
        $pagination = ($page - 1) * 20;
        $query = 'SELECT * FROM `Question` LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['theme'] = $themeDB->getThemeById($value['theme']);
                    $result[] = new \App\Entity\Question($value);
                }
    
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getFilteredByThemeQuestions(int $page, int $idTheme)
    {
        $themeDB = new ThemeDB();
        $pagination = ($page - 1) * 20;
        $query = 'SELECT * FROM `Question` WHERE theme LIKE :idTheme LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('idTheme', $idTheme, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['theme'] = $themeDB->getThemeById($value['theme']);
                    $result[] = new \App\Entity\Question($value);
                }
    
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSizeFilteredByTheme(int $idTheme)
    {
        $query = 'SELECT COUNT(*) FROM `Question` WHERE theme = :idTheme';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('idTheme', $idTheme, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result; 
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSize()
    {
        $query = 'SELECT COUNT(*) FROM `Question`';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    } 

    public function getQuestionById(int $id)
    {
        $themeDB = new ThemeDB();
        $query = 'SELECT * FROM `Question` WHERE id = ?';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute([$id]);

            if ($valid) {
                $result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);

                if ($result !== false) {
                    $result['theme'] = $themeDB->getThemeById($result['theme']);
                    return new Question($result);
                } else {
                    return null;
                }
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return null;
        }
    }

    public function getQuestionsByThemeId(int $themeId)
    {
        $themeDB = new ThemeDB();
        $query = 'SELECT * FROM `Question` WHERE theme=:id';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('id', $themeId, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = [];

                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['id'] = intval($value['id']);
                    $value['theme'] = $themeDB->getThemeById($value['theme']);
                    $result[] = new Question($value);
                }

                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return  false;
        }
    }
}
