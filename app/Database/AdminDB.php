<?php

namespace App\Database;

use App\Entity\Admin;
use PDOException;
use App\Core\Controller\AbstractController;

class AdminDB extends AbstractController
{
    private $pdo;
    private $pdoStatement;

    public function __construct()
    {
        $this->pdo = $this->getConnection();
    }

    public function addAdmin(Admin $admin): bool
    {
        $firstName = $admin->getFirstName();
        $lastName = $admin->getLastName();
        $email = $admin->getEmail();
        $registerAt = $admin->getRegisterAt()->format('Y-m-d H:i:s');
        $password_hash = password_hash($admin->getPassword(), PASSWORD_ARGON2I);

        $query = 'INSERT INTO Admin(firstName, lastName, email, registerAt, password)
        VALUES(:firstName, :lastName, :email, :registerAt, :password)';

        try {

            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('firstName', $firstName, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('lastName', $lastName, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('email', $email, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('registerAt', $registerAt, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('password', $password_hash, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function updateAdmin(Admin $admin): bool
    {
        $firstName = $admin->getFirstName();
        $lastName = $admin->getLastName();
        $email = $admin->getEmail();
        $id = $admin->getId();
        $password_hash = password_hash($admin->getPassword(), PASSWORD_ARGON2I);

        $query = 'UPDATE Admin 
                SET firstName=:firstName, lastName=:lastName, email=:email, password=:password
                WHERE id=:id';
        try {

            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('firstName', $firstName, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('lastName', $lastName, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('email', $email, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('password', $password_hash, \PDO::PARAM_STR);
       
            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function updateAdminWithoutChangingPassword(Admin $admin): bool
    {
        $firstName = $admin->getFirstName();
        $lastName = $admin->getLastName();
        $email = $admin->getEmail();
        $id = $admin->getId();

        $query = 'UPDATE Admin 
                SET firstName=:firstName, lastName=:lastName, email=:email
                WHERE id=:id';

        try {

            $this->pdoStatement = $this->pdo->prepare($query);
            
            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('firstName', $firstName, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('lastName', $lastName, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('email', $email, \PDO::PARAM_STR);
        
            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function deleteAdminById(int $id): bool {
        $query = 'DELETE FROM Admin WHERE id=:id';
        
        try {

            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function isPasswordCorrect(string $email, string $password): bool {
        $query = 'SELECT * FROM `Admin` WHERE email = ?';
        
        try {

            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute([$email]);
            
            if ($valid) {
                $admin = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);
                if (password_verify($password, $admin['password'])) {
                    return $valid;
                }
            }

            return false;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function isEmailUnique(string $email, int $id): bool {
        $query = 'SELECT * FROM `Admin` WHERE email = ? AND id <> ?';
        
        try {

            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute([$email, $id]);
            
            if ($valid) {
                if ($this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    return false;
                }
            }

            return true;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getAdminById(int $id)
    {
        $query = 'SELECT * FROM `Admin` WHERE id = ?';

        try {
            
            $this->pdoStatement = $this->pdo->prepare($query);
            $valid = $this->pdoStatement->execute([$id]);

            if ($valid) {
                $result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);

                if ($result !== false) {
                    $result['registerAt'] = new \DateTime($result['registerAt']);
                    return new Admin($result);
                } else {
                    return null;
                }
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    // returns array or bool
    public function getAll(int $page)
    {
        $pagination = ($page - 1) * 20;
        $query = 'SELECT * FROM `Admin` LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['registerAt'] = new \DateTime($value['registerAt']);
                    $result[] = new Admin($value);
                }

                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSize() 
    {
        $query = 'SELECT COUNT(*) FROM `Admin`';
        try {

            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute();

            if ($valid)  {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }
           
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }
}
