<?php

namespace App\Database;

use App\Core\Controller\AbstractController;
use App\Entity\User;
use PDOException;

class UserDB extends AbstractController
{
    private $pdo;
    private $pdoStatement;

    public function __construct()
    {
        $this->pdo = $this->getConnection();
    }

    public function getAll(int $page)
    {
        try {

            $pagination = ($page - 1) * 20;
            $query = 'SELECT * FROM `User` LIMIT 20 OFFSET :pagination';

            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['registerAt'] = new \DateTime($value['registerAt']);
                    $result[] = new User($value);
                }
    
                return $result;
            } else {
                return $valid;
            }
            
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }  
    }

    public function getUserById(int $id): ?User
    {
        $query = 'SELECT * FROM `User` WHERE id = ?';

        try {
            
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute([$id]);

            if ($valid) {
                $result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);

                if ($result !== false) {
                    $result['registerAt'] = new \DateTime($result['registerAt']);
                    return new User($result);
                } else {
                    return null;
                }
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSize()
    {
        $query = 'SELECT COUNT(*) FROM `User`';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }
            
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }
}
