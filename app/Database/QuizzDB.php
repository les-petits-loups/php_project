<?php

namespace App\Database;

use App\Core\Controller\AbstractController;
use App\Entity\Quizz;
use App\Database\UserDB;
use PDOException;

class QuizzDB extends AbstractController
{
    private $pdo;
    private $pdoStatement;

    public function __construct()
    {
        $this->pdo = $this->getConnection();
    }

    public function getAll(int $page)
    {
        $userDB = new UserDB();
        $pagination = ($page - 1) * 20;
        $query = 'SELECT * FROM `Quizz` LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];
            $values = []; 

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $values[] = $value;   
                }
    
                foreach($values as $value) {
                    $quizz = new Quizz([
                            'id' => $value['id'],
                            'mode' => $value['mode'],
                            'user1' => $userDB->getUserById($value['user1']),
                            'user2' => $value['user2'] === null ? null : $userDB->getUserById($value['user2']),
                            'winner' => $value['winner'] === null ? null : $userDB->getUserById($value['winner']),
                            'startAt' => new \DateTime($value['startAt']),
                            'questions' => $this->getQuizzQuestions($value['id'])
                    ]);
                    $result[] = $quizz;
                }
    
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getQuizzQuestions(int $idQuizz)
    {
        $query = 'SELECT * FROM `Quizz_Question` WHERE id_quizz = :idQuizz';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('idQuizz', $idQuizz, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $result[] = $value;
                }
    
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getQuizzsFiltered(int $page, String $filter)
    {
        $userDB = new UserDB();
        $pagination = ($page - 1) * 20;
        $filterTerm = $filter . "%";
        $query = 'SELECT Quizz.id, Quizz.mode, Quizz.user1, Quizz.user2, Quizz.winner, Quizz.startAt FROM Quizz, User  
                WHERE ( user1 = User.id OR user2 = User.id )
                AND ( ( firstName LIKE :filterTerm ) OR ( lastName LIKE :filterTerm )  OR ( email LIKE :filterTerm ) )
                LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('filterTerm', $filterTerm, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();
            $result = [];
            $values = []; 

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $values[] = $value;   
                }
    
                foreach($values as $value) {
                    $quizz = new Quizz([
                            'id' => $value['id'],
                            'mode' => $value['mode'],
                            'user1' => $userDB->getUserById($value['user1']),
                            'user2' => $value['user2'] === null ? null : $userDB->getUserById($value['user2']),
                            'winner' => $value['winner'] === null ? null : $userDB->getUserById($value['winner']),
                            'startAt' => new \DateTime($value['startAt']),
                            'questions' => $this->getQuizzQuestions($value['id'])
                    ]);
                    $result[] = $quizz;
                }
    
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSizeFiltered(String $filter) {
        $filterTerm = $filter . "%";
        $query = 'SELECT COUNT(*) FROM Quizz, User  
                WHERE ( user1 = User.id OR user2 = User.id )
                AND ( ( firstName LIKE :filterTerm ) OR ( lastName LIKE :filterTerm )  OR ( email LIKE :filterTerm ) )';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('filterTerm', $filterTerm, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result; 
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSize()
    {
        $query = 'SELECT COUNT(*) FROM `Quizz`';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    } 
}
