<?php

namespace App\Core\Controller;

use App\Core\Template\TemplateEngine;
use App\Core\Connection\Connection;
use Psr\Http\Message\ServerRequestInterface;

abstract class AbstractController
{
	protected ?ServerRequestInterface $request = null;
	
	protected function render(string $templatePath, array $params = []): string
	{
		$engine = TemplateEngine::instance();

		return $engine->render($templatePath, $params);
	}

	protected function getConnection(): \PDO
    {
        return Connection::getInstance();
	}
	
	public function getRequest(): ServerRequestInterface
    {
        return $this->request;
    }

    public function setRequest(ServerRequestInterface $request): AbstractController
    {
        $this->request = $request;

        return $this;
    }
}
