<?php
namespace App\Entity; 

class User extends BaseUser
{
  private int $points;

  public function __construct(array $props = [])
  {
    foreach ($props as $prop => $value) {
      $setter = 'set' . ucfirst(toCamelCase($prop));

      if (method_exists($this, $setter)) {
        $this->$setter($value);
      }
    }
  }

  // --- GETTERS ---
  public function getPoints(): int  { return $this->points; }

  // --- SETTERS ---
  public function setPoints(int $points): User
  {
    $this->points = $points;
    return $this;
  }
}
