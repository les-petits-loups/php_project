<?php
namespace App\Entity; 

abstract class BaseUser
{
  protected ?int $id = null;
  protected string $firstName;
  protected string $lastName;
  protected string $email;
  protected \DateTime $registerAt;
  protected string $password;

  // --- GETTERS ---
  public function getId(): ?int { return $this->id; }
  public function getFirstName(): string { return $this->firstName; }
  public function getLastName(): string { return $this->lastName; }
  public function getEmail(): string { return $this->email; }
  public function getRegisterAt(): \DateTime { return $this->registerAt; }
  public function getPassword(): string { return $this->password; }

  // --- SETTERS ---
  public function setId(int $id): BaseUser { 
    $this->id = $id; 
    return $this; 
  }

  public function setFirstName(string $firstName): BaseUser {
     $this->firstName = $firstName; 
     return $this; 
  }
  
  public function setPassword(string $password): BaseUser
  {
    $this->password = $password;
    return $this;
  }

  public function setLastName(string $lastName): BaseUser
  {
    $this->lastName = $lastName;
    return $this;
  }

  public function setEmail(string $email): BaseUser
  {
    $this->email = $email;
    return $this;
  }

  public function setRegisterAt(\DateTime $registerAt): BaseUser
  {
    $this->registerAt = $registerAt;
    return $this;
  }

  
}
