<?php
namespace App\Entity;

class Admin extends BaseUser
{
    public function __construct(array $props = [])
    {
        foreach ($props as $prop => $value) {
            $setter = 'set' . ucfirst(toCamelCase($prop));

            if (method_exists($this, $setter)) {
                $this->$setter($value);
            }
        }
    }


}
