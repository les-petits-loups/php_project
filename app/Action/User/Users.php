<?php

namespace App\Action\User;

use App\Core\Controller\AbstractController;
use App\Database\UserDB;

class Users extends AbstractController
{
    public function __invoke(int $curr_page = 1)
    {
        $userDB = new UserDB();
        
        $total_records = (int) $userDB->getTableSize();
        if ($total_records === false) { header('Location: /error'); }
        $total_pages = (int) ceil($total_records / 20);

        if ($curr_page <= 0 || $curr_page > $total_pages) {
			$curr_page = 1;
        }
        
        $users = $userDB->getAll($curr_page);
        if ($users === false) { header('Location: /error'); }
        

        return $this->render(
            'user/users.html.twig',
            [
                'curr_page' => $curr_page,
                'total_pages' => $total_pages,
                'users' => $users,
            ]
        );
    }
}
