<?php

namespace App\Action\Quizz;

use App\Core\Controller\AbstractController;
use App\Database\QuizzDB;

class Quizzs extends AbstractController
{
    public function __invoke(int $curr_page = 1)
    {
        $quizzDB = new QuizzDB();

        if (empty($_GET['filter'])) {

            $total_records = (int) $quizzDB->getTableSize();
            if ($total_records === false) { header('Location: /error'); }
            $total_pages = (int) ceil($total_records / 20);

            if ($curr_page <= 0 || $curr_page > $total_pages) {
                $curr_page = 1;
            }

            $quizzs = $quizzDB->getAll($curr_page);$quizzs = $quizzDB->getAll($curr_page);
            if ($quizzs === false) { header('Location: /error'); }
                
        } else {
    
            $total_records = (int) $quizzDB->getTableSizeFiltered($_GET['filter']);
            if ($total_records === false) { header('Location: /error'); } 
            $total_pages = (int) ceil($total_records / 20);

            if ($curr_page <= 0 || $curr_page > $total_pages) {
                $curr_page = 1;
            }

            $quizzs = $quizzDB->getQuizzsFiltered($curr_page, $_GET['filter']);      
            if ($quizzs === false) { header('Location: /error'); }
        }
        
        return $this->render(
            'quizz/quizzs.html.twig',
            [
                'curr_page' => $curr_page,
                'total_pages' => $total_pages,
                'quizzs' => $quizzs,
            ]
        );
    }
}
