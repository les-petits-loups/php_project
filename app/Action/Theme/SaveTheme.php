<?php

namespace App\Action\Theme;

use App\Database\ThemeDB;
use App\Entity\Theme;
use App\Core\Controller\AbstractController;
use App\Validator\Validator;

class SaveTheme extends AbstractController
{
    // if $id !== 0 : updating already existing theme
    public function __invoke(int $id = 0)
    {
        $themeDB = new themeDB();
        $theme = new theme([
            "name" => $_POST['name'],
        ]);
        
        if ($id !== 0) { $theme->setId($id); }

        $validator = new Validator();
        $isValid = $validator->validate($theme);
        
        try {
            if ($id !== 0 && $isValid) {
                $result = $themeDB->updateTheme($theme);
                if ($result === false) { header('Location: /error'); }
                header('Location: /themes');
            } else if ($id === 0 && $isValid) { 
                $result = $themeDB->addTheme($theme);
                if ($result === false) { header('Location: /error'); }
                header('Location: /themes');
            } else {
                // re-rendering same template, but with errors displayed
                $errors = $validator->getErrors();
                return $this->render(
                    'theme/theme.html.twig',
                    [
                        'theme' => $theme,
                        'errors' => $errors,
                    ]
                );
            }
        } catch (\Exception $e) {
            header('Location: /error');
        }
        
    }
}
