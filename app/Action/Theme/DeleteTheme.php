<?php

namespace App\Action\Theme;

use App\Core\Controller\AbstractController;
use App\Database\ThemeDB;

class DeleteTheme extends AbstractController
{
    public function __invoke(int $themeId)
    {
        $themeDB = new themeDB();
        $theme = $themeDB->getThemeById($themeId);

        if ($theme !== false && $themeDB->deleteThemeById($themeId)) {
            if ($this->getRequest()->getMethod() === 'GET') {
                header('Location: /themes');
                exit(0);
            }
            exit(0);
        } else {
            header('Location: /error');
        }
    
    }
}