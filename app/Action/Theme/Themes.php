<?php

namespace App\Action\Theme;
use App\Core\Controller\AbstractController;
use App\Database\ThemeDB;

class Themes extends AbstractController {

    public function __invoke(int $curr_page = 1) {

        $themeDB = new ThemeDB();

        $total_records = (int) $themeDB->getTableSize();

        if ($total_records !== false) {
            $total_pages = (int) ceil($total_records / 20);
          
            if ($curr_page <= 0 || $curr_page > $total_pages) {
              $curr_page = 1;
            }

            $themes = $themeDB->getAll($curr_page); 
            if ($themes === false) { header('Location: /error'); }

            return $this->render(
                'theme/themes.html.twig',
                [
                    'curr_page' => $curr_page,
                    'total_pages' => $total_pages,
                    'themes' => $themes,
                ]
          );
        } else {
            header('Location: /error');
        }
    }
}
