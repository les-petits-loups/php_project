<?php

namespace App\Action\Theme;

use App\Database\ThemeDB;
use App\Core\Controller\AbstractController;

class Theme extends AbstractController
{
    public function __invoke($id = 0)
    {
        $theme = (new ThemeDB)->getThemeById((int) $id);
        
        if ($theme !== false) {
            return $this->render(
              'theme/theme.html.twig',
              [
                  'theme' => $theme,
              ]
            );
        } else {
            header('Location: /error');
        }
    }
}
