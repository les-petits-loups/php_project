<?php

namespace App\Action\Theme;

use App\Core\Controller\AbstractController;
use App\Database\ThemeDB;

class FetchTheme extends AbstractController
{
    public function __invoke()
    {
        $term = !empty($_GET['search']) ? $_GET['search'] : "";
        $page = $_GET['page'];
        $themeDB = new themeDB();

        $themes = $themeDB->getPaginatedThemes($term, $page);
        if ($themes !== false) {
            return json_encode($themes, JSON_PRETTY_PRINT);
        } else {
            header('Location: /error');
        }    
    }
}