<?php

namespace App\Action\Admin;

use App\Database\AdminDB;
use App\Entity\Admin;
use App\Core\Controller\AbstractController;
use App\Validator\Validator;

class SaveAdmin extends AbstractController
{
    // if $id !== 0 : updating already existing user
    public function __invoke($id = 0)
    {
        try {

            $adminDB = new AdminDB();
            $admin = new Admin([
                "firstName" => $_POST['firstName'],
                "lastName" => $_POST['lastName'],
                "email" => $_POST['email'],
                "registerAt" => new \DateTime('today'),
                "password" => $_POST['password']
            ]);
            
            if ($id !== 0) { 
                $oldAdmin = $adminDB->getAdminById($id);
                if ($oldAdmin === false) { header('Location: /error'); }

                $admin->setId($oldAdmin->getId());
                $admin->setRegisterAt($oldAdmin->getRegisterAt());
            }
            
            $passwordConfirmationError = $_POST['password'] !== $_POST['confirmPassword'];
            $ignorePasswordVerification = false;
            $isCurrentPasswordCorrect = true;
            $isTryingToChangePassword = (
                (!empty($_POST['currentPassword']) &&  $_POST['currentPassword'] !== "") 
                || $_POST['password'] !== "" 
                || $_POST['confirmPassword'] !== ""
            );
            

            if ($isTryingToChangePassword && $id !== 0) {
                $isCurrentPasswordCorrect = $adminDB->isPasswordCorrect($oldAdmin->getEmail(), $_POST['currentPassword']);
            } else if (!$isTryingToChangePassword && $id !== 0) {
                $ignorePasswordVerification = true;
            }

            $validator = new Validator();
            $isValid = $validator->validate($admin, $ignorePasswordVerification);
            
        
            if ($id !== 0 && $isValid && $isTryingToChangePassword && $isCurrentPasswordCorrect && !$passwordConfirmationError) {
                $result = $adminDB->updateAdmin($admin);
                if ($result !== true) { header('Location: /error'); }
                header('Location: /admins');

            } else if ($id !== 0 && $isValid && !$isTryingToChangePassword) {
                $result = $adminDB->updateAdminWithoutChangingPassword($admin);
                if ($result !== true) { header('Location: /error'); }
                header('Location: /admins');

            } else if ($id === 0 && $isValid && !$passwordConfirmationError) { 
                $result = $adminDB->addAdmin($admin);
                if ($result !== true) { header('Location: /error'); }
                header('Location: /admins');

            } else {
                // re-rendering same template, but with errors displayed
                $errors = $validator->getErrors();
                return $this->render(
                    'admin/admin.html.twig',
                    [
                        'admin' => $admin,
                        'errors' => $errors,
                        'isTryingToChangePassword' => $isTryingToChangePassword,
                        'passwordConfirmationError' => $passwordConfirmationError,
                        'isCurrentPasswordCorrect' => $isCurrentPasswordCorrect,
                    ]
                );
            }
        } catch (\Exception $e) {
            header('Location: /error');
        }
    }
}
