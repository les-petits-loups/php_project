<?php

namespace App\Action\Errors;

use App\Core\Controller\AbstractController;

class _404 extends AbstractController
{
    public function __invoke()
    {
        return $this->render('errors/error.html.twig', [
            "errorCode" => "404",
            "message" => "La page que vous recherchez a peut-être été supprimée ou est indisponible."
        ]);
    }
}
