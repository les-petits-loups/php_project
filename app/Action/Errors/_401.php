<?php

namespace App\Action\Errors;

use App\Core\Controller\AbstractController;

class _401 extends AbstractController
{
    public function __invoke()
    {
        return $this->render('errors/error.html.twig', [
            "errorCode" => "401",
            "message" => "Méthode non autorisée"
        ]);
    }
}
