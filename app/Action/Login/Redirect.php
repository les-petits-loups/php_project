<?php

namespace App\Action\Login;

use App\Database\LoginDB;
use App\Core\Controller\AbstractController;

class Redirect extends AbstractController
{
	public function __invoke()
	{
		if (isset($_POST['inputEmail']) && isset($_POST['inputPassword'])) {
			$loginDB = new LoginDB();
			$isLoginInfoCorrect = $loginDB->isLoginInfoCorrect($_POST['inputEmail'], $_POST['inputPassword']);
			if ($isLoginInfoCorrect) {
				if (isset($_POST['rememberPasswordCheck'])) {
					$loginDB->rememberMe();
				}
				?>
				<script>
					setTimeout(function() {
						window.location.href = '/';
					}, 5000);
				</script>
				<?php
				return $this->render("login/redirect.html.twig", 
				[
					'messages' => ['Vous êtes connecté.', 'Vous allez être redirigé vers l\'accueil dans 5 secondes.'],
				]);
			} else {
				$location = "Location: /login?error=true&email=" . $_POST['inputEmail'];
				header($location);
			}
		} else {
			header("Location: /");
		}
	}
}
