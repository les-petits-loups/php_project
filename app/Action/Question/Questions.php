<?php

namespace App\Action\Question;

use App\Core\Controller\AbstractController;
use App\Database\QuestionDB;
use App\Database\ThemeDB;

class Questions extends AbstractController
{
    public function __invoke(int $curr_page = 1)
    {
        $questionDB = new QuestionDB();

        if (!empty($_GET['theme']) && $_GET['theme'] !== 0) {

            $themeDB = new ThemeDB();
            $themeId = $_GET['theme'];
            $theme = $themeDB->getThemeById($themeId);

            if ($theme !== false) {

                $themeName = $theme->getName();

                $total_records = (int) $questionDB->getTableSizeFilteredByTheme($themeId);
                $total_pages = (int) ceil($total_records / 20);

                if ($curr_page <= 0 || $curr_page > $total_pages) {
                    $curr_page = 1;
                }

            } else {
                header('Location: /error');
            }

            $questions = $questionDB->getFilteredByThemeQuestions($curr_page, $themeId);
            
        } else {

            $total_records = (int) $questionDB->getTableSize();
            $total_pages = (int) ceil($total_records / 20);

            if ($curr_page <= 0 || $curr_page > $total_pages) {
                $curr_page = 1;
            }

            $questions = $questionDB->getAll($curr_page);
        }

        if ($questions !== false) {
            return $this->render(
                'question/questions.html.twig',
                [
                    'curr_page' => $curr_page,
                    'total_pages' => $total_pages,
                    'questions' => $questions,
                    'filteredByTheme' => !empty($_GET['theme']) ? 'true' : 'false',
                    'filteredByThemeId' => !empty($themeId) ? $themeId : 0,
                    'filteredByThemeName' => !empty($themeName) ? $themeName : 0,
                ]
            );
        } else {
            header('Location: /error');
        }

        
    }
}
