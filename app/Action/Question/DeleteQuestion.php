<?php

namespace App\Action\Question;

use App\Core\Controller\AbstractController;
use App\Database\QuestionDB;

class DeleteQuestion extends AbstractController
{
    public function __invoke(int $questionId)
    {
        $questionDB = new QuestionDB();
        $question = $questionDB->getQuestionById($questionId);

        if ($question !== false && $questionDB->deleteQuestionById($questionId)) {
            if ($this->getRequest()->getMethod() === 'GET') {
                header('Location: /questions');
                exit(0);
            }
            exit(0);
        } else {
            header('Location: /error');
        }
    }
}