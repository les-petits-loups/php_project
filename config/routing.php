<?php

use App\Action\Home;
use App\Action\User\Users;
use App\Action\Quizz\Quizzs;
use App\Action\Admin\Admin;
use App\Action\Admin\SaveAdmin;
use App\Action\Admin\DeleteAdmin;
use App\Action\Admin\Admins;
use App\Action\Theme\Themes;
use App\Action\Theme\Theme;
use App\Action\Theme\SaveTheme;
use App\Action\Theme\DeleteTheme;
use App\Action\Theme\FetchTheme;
use App\Action\Login\Login;
use App\Action\Login\Logout;
use App\Action\Login\Redirect;
use App\Action\Question\Questions;
use App\Action\Question\Question;
use App\Action\Question\SaveQuestion;
use App\Action\Question\DeleteQuestion;
use App\Core\Routing\Route;
use App\Action\Errors\_404;
use App\Action\Errors\_401;
use App\Action\Errors\_AnonymousError;

return [
    // HOME
    new Route('/', Home::class, 'GET'),

    // ADMINS
    new Route('/admin/delete/{adminId}', DeleteAdmin::class,  'DELETE'),
    new Route('/admin/save[/[{adminId}]]', SaveAdmin::class, 'POST'),
    new Route('/admin/new', Admin::class, 'GET'),

    new Route('/admin/{adminId}', Admin::class, 'GET'),
    new Route('/admins[/{page}]', Admins::class, 'GET'),

    // USERS
    new Route('/users[/{page}]', Users::class, 'GET'),

    // QUIZZS
    new Route('/quizzs[/{page}]', Quizzs::class, 'GET'),

    // THEMES
    new Route('/theme/delete/{themeId}', DeleteTheme::class,  'DELETE'),
    new Route('/theme/save[/[{themeId}]]', SaveTheme::class, 'POST'),
    new Route('/themes/paginated', FetchTheme::class,  'GET'),
    new Route('/theme/new', Theme::class, 'GET'),

    new Route('/theme/{themeId}', Theme::class, 'GET'),
    new Route('/themes[/{page}]', Themes::class,  'GET'),
 
    // LOGIN
    new Route('/redirect', Redirect::class, ['GET', 'POST']),
    new Route('/logout', Logout::class, 'GET'),
    new Route('/login', Login::class, 'GET'),

    // QUESTIONS
    new Route('/question/delete/{questionId}', DeleteQuestion::class,  'DELETE'),
    new Route('/question/save[/[{questionId}]]', SaveQuestion::class, 'POST'),
    new Route('/question/new', Question::class, 'GET'),

    new Route('/question/{questionId}', Question::class, 'GET'),
    new Route('/questions[/{page}]', Questions::class, 'GET'),

    // ERRORS
    new Route('/404', _404::class, 'GET'),
    new Route('/401', _401::class, 'GET'),
    new Route('/error', _AnonymousError::class, 'GET'),
];