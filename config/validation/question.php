<?php
return [
    'label' => [
        [
            'rule' => 'not_blank',
            'message' => 'La question ne peut pas être vide'
        ],
        [
            'rule' => 'length',
            'max' => 255,
            'min' => 3,
            'message' => 'La question doit comporter entre 3 et 255 caractères'
        ],
        [
            'rule' => 'unique_question',
            'message' => 'La question existe déjà'
        ]
    ],
    'theme' => [
        [
            'rule' => 'has_id',
            'message' => 'Le thème ne peut pas être vide'
        ]
    ]

];