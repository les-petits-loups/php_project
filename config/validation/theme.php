<?php
return [
    'name' => [
        [
            'rule' => 'not_blank',
            'message' => 'Le nom ne peut pas être vide'
        ],
        [
            'rule' => 'length',
            'max' => 255,
            'min' => 3,
            'message' => 'Le nom doit comporter entre 3 et 255 caractères'
        ],
        [
            'rule' => 'unique_theme',
            'message' => 'Ce thème existe déjà'
        ]
    ]
];
