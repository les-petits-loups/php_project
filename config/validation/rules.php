<?php

use App\Entity\User;
use App\Entity\Admin;
use App\Entity\Theme;
use App\Entity\PossibleAnswer;
use App\Entity\Question;

return [
    User::class => require __DIR__ . '/user.php',
    Admin::class => require __DIR__ . '/admin.php',
    Theme::class => require __DIR__ . '/theme.php',
    Question::class => require __DIR__ . '/question.php',
    PossibleAnswer::class => require __DIR__ . '/possibleAnswer.php'
];
