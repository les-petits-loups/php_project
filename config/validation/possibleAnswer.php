<?php
return [
    'label' => [
        [
            'rule' => 'not_blank',
            'message' => 'La réponse ne peut pas être vide'
        ],
        [
            'rule' => 'length',
            'max' => 255,
            'min' => 3,
            'message' => 'La réponse doit comporter entre 3 et 255 caractères'
        ]
    ]
];