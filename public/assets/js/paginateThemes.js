const select2 = $("#js-data-fetch-theme").select2({
    language: {
        searching: function () {
            return "Recherche en cours...";
        },

        noResults: function () {
            return "Aucun résultat trouvé";
        },
    },
    placeholder: "",
    ajax: {
        delay: 500,
        url: "/themes/paginated",
        dataType: "json",
        width: "resolve",
        data: function (params) {
            const query = {
                search: params.term,
                page: params.page || 1,
            };

            // Query parameters will be ?search=[term]&page=[page]
            return query;
        },
        processResults: function (data, params) {
            params.page = params.page || 1;
            return {
                results: data.results.map(function (obj) {
                    obj.text = obj.name;
                    return obj;
                }),
                pagination: {
                    more: data.count > 20 * params.page,
                },
            };
        },
    },
});

select2.focus(function () { $(this).select2('open'); });
select2.data('select2').$container.addClass("form-control");
select2.data('select2').dropdown.$search.addClass("form-control");
